import { Datastore } from 'nedb';

const db = new Datastore({ filename: '../database/db', autoload: true });

const doc = {
  hello: 'world',
  n: 5,
  today: new Date(),
  nedbIsAwesome: true,
  notthere: null,
  notToBeSaved: undefined, // Will not be saved
  fruits: [ 'apple', 'orange', 'pear' ],
  infos: { name: 'nedb' },
};

db.insert(doc, function(err, newDoc) {
  if (err) {
    return console.log(err);
  }
  console.log(newDoc);
  // newDoc is the newly inserted document, including its _id
  // newDoc has no key called notToBeSaved since its value was undefined
});
