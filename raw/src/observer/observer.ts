function Observers() {
  this.observers = [];
}

Observers.prototype.add = (obj) => {
  return this.observers.push(obj);
};

Observers.prototype.get = (idx: number) => {
  if (idx > -1 && idx < this.observers.length) {
    return this.observers[idx];
  }
};

Observers.prototype.indexOf = (obj, startIdx: number) => {
  const i = startIdx;
  while (i < this.observers.length) {
    if (this.observers[i] === obj) {
      return i;
    }
    i++;
  }
  return -1;
};

Observers.prototype.removeAt = function(idx) {
  this.observers.splice(idx, 1);
};

// The Observer
function Observer() {
  this.update = function() {
    // ...
  };
}
