// Obj literal (Comma separate key/val)
const o1 = {
  key: 'val',
  func: () => {
    return 'sup';
  },
};

o1.func();

// module pattern
// self-container
const myMod = () => {
  const count = 0;

  return {
    increment: () => {
      return counter++;
    },
    reset: () => {
      count = 0;
    },
  };
};

myMod.increment();
myMod.reset();

// it can do namespace

const namespace = (() => {
  const name, age;
    name = false;
    age = false;

    privMethod = (foo) => {
      console.log(foo)
    };

    return {
      pub: 'foo',

      pubMethod: (baz) => {
        age++
        privMEthod(baz);
      }
    }
})

// SINGLETON

const single = (() => {
  const inst;

  function init() {
    function privMethod() {
      console.log('im priv')
    }

    const privProp = 'im secret'
    const privNum = 2;

    return {
      publMethod: () => {
        console.log('pub see me')
      },
      pubProp: 'im pub',
    }
  }

  return {
    getInstance = function() {
      if ( this._inst == null) {
        this.inst = new this;
      }
      if ( !inst ) {
        inst = init();
      }
      return this.inst;
    }
  }
})


const s1 = single.getInstance();
const s2 = single.getInstance();
