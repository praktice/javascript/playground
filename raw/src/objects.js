import { RefCountDisposable } from 'rx';

// Create
const o1 = {};
const o2 = Object.create(Object.prototype);
const o3 = new Object();
const o4 = {}; // an extra

// Properties
o1.key = 'hi';
console.log(o1.key);

o2['key'] = 'hi';
console.log(o2.key);

Object.defineProperty(o3, 'key', {
  value: 'lol, this is detailed',
  writable: true,
  enumerable: true,
  configurable: true,
});
// or
const o5 = {};
Object.defineProp(o5, 'key', 'hi');
Object.defineProp(o5, 'key', 'sup');

Object.defineProperties(o4, {
  key1: {
    value: 'sup',
    writable: true,
  },
  key2: {
    value: 'yo?',
    writable: false,
  },
});

// Prototypes are available on creation
class Animal {

  constructor(type) {
    this.type = type;
  }

}

Animal.prototype.toStrinng = () => {
  return this.type + ' is an animal';
};

const a = new Animal('Pug');
const b = new Animal('Husky');
console.log(a.toStrinng());
console.log(b.toStrinng());
