const require('../src/MethChain');
const canvas = document.querySelector('#stage'),
  ctx = new MethChain(canvas.getContext('2d'));

ctx
  .set('strokeStyle', '#fe11a5')
  .arc(50, 50, 25, 0, Math.PI * 2)
  .stroke();
