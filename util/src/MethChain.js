const MethChain = (() => {
  function MethChain(obj) {
    if (!(this instanceof MethChain)) {
      return new MethChain(...arguments);
    }

    this.obj = obj;

    for (const method in obj) {
      if (typeof obj[method] === 'function') {
        this[method] = (...args) => {
          this.obj[method](...args);
          return this;
        };
      }
    }
  }

  MethChain.proto = MethChain.prototype;

  /**
   * @method set
   * @description converts a setter into a function
   * @param {string} prop
   * @param {string} val
   */
  MethChain.proto.set = function(prop, val) {
    this.obj[prop] = val;
    return this;
  };
  return MethChain;
};

exports.MethChain;
